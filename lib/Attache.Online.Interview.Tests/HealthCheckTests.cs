﻿using System;
using System.Collections.Generic;
using System.Threading;
using Attache.Online.Services.Interview.HealthCheck.Checks;
using Microsoft.Extensions.HealthChecks;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using Serilog.Events;
using Serilog.Parsing;
using Shouldly;

namespace Attache.Online.Interview.Tests
{
    [TestFixture]
    public class HealthCheckTests
    {
        private RabbitMqHealthCheck rabbitMqHealthCheck;
        private CancellationTokenSource cancellationToken;

        [SetUp]
        public void Setup()
        {
            cancellationToken = new CancellationTokenSource();
            rabbitMqHealthCheck = Substitute.ForPartsOf<RabbitMqHealthCheck>(cancellationToken);
        }

        [TestCase("RabbitMQ Connect Failed: Operation interruptedappuser@rabbitmq.prod.aws.attachecloud.com:5672")]
        [TestCase("RabbitMQ Connect Failed: Broker unreachable: guest@localhost:5672/")]
        public void GivenLogContainsUnhealthyTrigger_ResultShouldBeUnhealthy(string log)
        {
            rabbitMqHealthCheck.Emit(ToLogEvent(log));
            rabbitMqHealthCheck.CheckAsync().Result.CheckStatus.ShouldBe(CheckStatus.Unhealthy);
        }

        private LogEvent ToLogEvent(string log)
        {
            return new LogEvent(DateTimeOffset.Now, LogEventLevel.Error, null, new MessageInterview(log, new List<MessageInterviewToken>()), new List<LogEventProperty>());
        }

        [TestCase("EmployeeDomainRepository -> UpSertEmployeeTable for employeeId: 5f3e21bd-67c4-f044-b64c-3933e769ee88")]
        [TestCase("Memcached server address - cache3.localtest.me(127.0.0.1):11211")]
        public void GivenLogDoesNotContainUnhealthyTrigger_ResultShouldBeHealthy(string log)
        {
            rabbitMqHealthCheck.Emit(ToLogEvent(log));
            rabbitMqHealthCheck.CheckAsync().Result.CheckStatus.ShouldBe(CheckStatus.Healthy);
        }

        [TestCase("RabbitMQ Connect Failed: Operation interruptedappuser@rabbitmq.prod.aws.attachecloud.com:5672")]
        [TestCase("RabbitMQ Connect Failed: Broker unreachable: guest@localhost:5672/")]
        public void GivenLogContainsUnhealthyTrigger_RecoveryFuncShouldBeCalled(string log)
        {
            rabbitMqHealthCheck.Emit(ToLogEvent(log));
            rabbitMqHealthCheck.Received().AttemptToRecover();
        }

        [TestCase("RabbitMQ Connect Failed: Broker unreachable: guest@localhost:5672/")]
        public void GivenLogContainsUnhealthyTrigger_AndRecoveryFuncSucceeded_ResultShouldBeHealthy(string log)
        {
            rabbitMqHealthCheck.AttemptToRecover().Returns(true);

            rabbitMqHealthCheck.Emit(ToLogEvent(log));

            rabbitMqHealthCheck.CheckAsync().Result.CheckStatus.ShouldBe(CheckStatus.Healthy);
        }

        [TestCase("RabbitMQ Connect Failed: Broker unreachable: guest@localhost:5672/")]
        public void GivenLogContainsUnhealthyTrigger_AndRecoveryFuncFailed_ResultShouldBeUnhealthy(string log)
        {
            rabbitMqHealthCheck.AttemptToRecover().Returns(false);

            rabbitMqHealthCheck.Emit(ToLogEvent(log));

            rabbitMqHealthCheck.CheckAsync().Result.CheckStatus.ShouldBe(CheckStatus.Unhealthy);
        }

        [TestCase("RabbitMQ Connect Failed: Broker unreachable: guest@localhost:5672/")]
        public void GivenLogContainsUnhealthyTrigger_AndRecoveryFuncThrewAnException_ResultShouldBeUnhealthy(string log)
        {
            rabbitMqHealthCheck.AttemptToRecover().Throws(new Exception());

            rabbitMqHealthCheck.Emit(ToLogEvent(log));

            rabbitMqHealthCheck.CheckAsync().Result.CheckStatus.ShouldBe(CheckStatus.Unhealthy);
        }
    }
}
