using System;
using Attache.Online.Services.Interview.HealthCheck;
using Attache.Online.Services.Interview.HealthCheck.Checks;
using Autofac;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using Shouldly;

namespace Attache.Online.Interview.Tests
{
    [TestFixture]
    public class IocConfigTests
    {
        ContainerBuilder builder;
        ILifetimeScope scope;
        ILifetimeScope scope2;

        [SetUp]
        public void SetUp()
        {
            builder = new ContainerBuilder();
        }

        private void WhenBuildServicesContainer()
        {
            builder.RegisterModule(new Services.Interview.IoC.InterviewServicesModule());

            builder.RegisterGeneric(typeof(MockLogger<>)).As(typeof(ILogger<>));

            builder.RegisterInstance(new LoggerFactory()).AsImplementedInterfaces();

            var container = builder.Build();
            scope = container.BeginLifetimeScope();
            scope2 = container.BeginLifetimeScope();
        }

        [Test]
        public void CanResolveServices()
        {
            WhenBuildServicesContainer();

            // Health Checks
            ShouldResolveService<RabbitMqHealthCheck>();
            ShouldBeSingleton(typeof(RabbitMqHealthCheck));
            ShouldResolveService<SerilogHealthCheck>();
        }

        void ShouldResolveService<T>()
        {
            scope.Resolve<T>().ShouldNotBeNull();
        }

        void ShouldBeTransient(Type type)
        {
            scope.Resolve(type).ShouldNotBe(scope2.Resolve(type));
        }

        void ShouldBeSingleton(Type type)
        {
            scope.Resolve(type).ShouldBe(scope2.Resolve(type));
        }
    }

    public class MockLogger<T> : ILogger<T>
    {
        public IDisposable BeginScope<TState>(TState state)
        {
            throw new NotImplementedException();
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            throw new NotImplementedException();
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            throw new NotImplementedException();
        }
    }
}
