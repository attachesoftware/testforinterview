﻿using System.Linq;
using Attache.Online.Interview.ApplicationServices.Contracts.Queries;
using NUnit.Framework;
using Shouldly;

namespace Attache.Online.Interview.Infrastructure.Tests
{
    [TestFixture]
    public class ContractsReferenceTests
    {
        [Test]
        public void ValidateReferences()
        {
            /*
             * If this test is failing for you, you've added an assembly reference to this library that is not permitted.
             * Undo this immediately and rethink your strategy.
             */
            var assemblies = typeof(SampleQuery).Assembly.GetReferencedAssemblies();

            assemblies.Where(a => a.Name.StartsWith("Attache") && !a.Name.Equals("Attache.Messaging")).Count().ShouldBe(0, 
                "DO NOT reference ANY libraries in your contracts. These should be plain POCO. Especially do not reference your domain objects here");
        }
    }
}