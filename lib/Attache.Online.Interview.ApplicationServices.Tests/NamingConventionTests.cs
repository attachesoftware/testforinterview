﻿using Attache.Messaging;
using Attache.Online.Interview.ApplicationServices.Contracts.Queries;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Attache.Online.Interview.ApplicationServices.Tests
{
    [TestFixture]
    public class NamingConventionTests
    {
        [Test]
        public void ImplementationsShouldHaveNamespace()
        {
            Assembly mscorlib = typeof(SampleQuery).Assembly;
            foreach (Type type in mscorlib.GetTypes())
            {
                CheckNamespace(type);
            }
        }

        private void CheckNamespace(Type type)
        {
            if (typeof(IQuery).IsAssignableFrom(type))
            {
                Assert.AreEqual("Attache.Online.Interview.ApplicationServices.Contracts.Queries", type.Namespace, string.Format("Type {0} should be in the namespace Attache.Online.Interview.ApplicationServices.Contracts.Queries", type.Name));
            }
            else if (typeof(ICommand).IsAssignableFrom(type))
            {
                Assert.AreEqual("Attache.Online.Interview.ApplicationServices.Contracts.Commands", type.Namespace, string.Format("Type {0} should be in the namespace Attache.Online.Interview.ApplicationServices.Contracts.Commands", type.Name));
            }
            else if (typeof(IEvent).IsAssignableFrom(type))
            {
                Assert.AreEqual("Attache.Online.Interview.ApplicationServices.Contracts.Events", type.Namespace, string.Format("Type {0} should be in the namespace Attache.Online.Interview.ApplicationServices.Contracts.Events", type.Name));
            }
        }

        private bool IsSubclassOfRawGeneric(Type generic, Type toCheck)
        {
            while (toCheck != null && toCheck != typeof(object))
            {
                var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur)
                {
                    return true;
                }
                toCheck = toCheck.BaseType;
            }
            return false;
        }
    }
}
