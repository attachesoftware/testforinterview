﻿using Attache.Messaging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Attache.Online.Interview.ApplicationServices.Contracts.Commands
{
    [Feature("sample", "feature")]
    public class DoSample : ICommand, IOrganisationMessage
    {
        public Guid OrganisationId { get; set; }
    }
}
