﻿using Attache.Messaging;

namespace Attache.Online.Interview.ApplicationServices.Contracts.Queries
{
    [AllowAnonymous]
    public class HealthCheckQuery : IQuery
    {
    }

    public class HealthCheckQueryRepsonse : IMessage
    {
        public const string Success = "Success";
        public string Message { get; set; }
    }
}