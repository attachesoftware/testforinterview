﻿using Attache.Messaging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Attache.Online.Interview.ApplicationServices.Contracts.Queries
{
    [Feature("sample", "feature")]
    public class SampleQuery : IQuery, IOrganisationMessage
    {
        public Guid OrganisationId { get; set; }
    }

    public class SampleQueryResponse : IMessage
    {
        public string Response { get; set; }
    }
}
