﻿using Attache.Online.Interview.ApplicationServices.Contracts.Queries;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Attache.Online.Interview.ApplicationServices.QueryHandlers.Sample
{
    public class SampleQueryHandler :
        IConsumer<SampleQuery>
    {
        public async Task Consume(ConsumeContext<SampleQuery> context)
        {
            await context.RespondAsync(new SampleQueryResponse() { Response = "OK" });
        }
    }
}
