﻿using Attache.Messaging;
using Attache.Online.Authentication;
using Attache.Online.Interview.ApplicationServices.Contracts.Events;
using Attache.Online.Interview.ApplicationServices.Contracts.Commands;
using Attache.Online.Interview.ApplicationServices.Contracts.Queries;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Attache.Online.Interview.ApplicationServices.CommandHandlers.Sample
{
    public class SampleCommandHandler : IConsumer<DoSample>
    {
        private ICommandQueryEventBus commandQueryEventBus;
        private ServiceCredentialStore serviceCredentialStore;

        public SampleCommandHandler(ICommandQueryEventBus commandQueryEventBus, ServiceCredentialStore serviceCredentialStore)
        {
            this.commandQueryEventBus = commandQueryEventBus;
            this.serviceCredentialStore = serviceCredentialStore;
        }

        public async Task Consume(ConsumeContext<DoSample> context)
        {
            Console.WriteLine("ok");

            var sampleQuery = new SampleQuery() { OrganisationId = context.Message.OrganisationId };

            // Events don't require credentials
            await commandQueryEventBus.RaiseEvent(new SampleEvent());

            // Query using the calling context credentials
            await commandQueryEventBus.UsingCredentials(context).ExecuteQuery<SampleQuery, SampleQueryResponse>(sampleQuery);

            // Query using the service credentials
            await commandQueryEventBus.UsingCredentials(serviceCredentialStore).ExecuteQuery<SampleQuery, SampleQueryResponse>(sampleQuery);

        }
    }
}
