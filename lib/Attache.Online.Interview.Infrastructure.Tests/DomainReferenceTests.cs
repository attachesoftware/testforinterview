﻿using System.Linq;
using NUnit.Framework;
using Shouldly;

namespace Attache.Online.Interview.Infrastructure.Tests
{
    [TestFixture]
    public class ReferenceTests
    {
        [Test]
        public void ValidateReferences()
        {
            /*
             * If this test is failing for you, you've added an assembly reference to this library that is not permitted.
             * Undo this immediately and rethink your strategy.
             */
            var assemblies = typeof(AutoMappings).Assembly.GetReferencedAssemblies();

            assemblies.FirstOrDefault(a => a.Name == "Attache.Online.MX").ShouldBeNull("DO NOT reference MX library in your domain");
        }
    }
}