﻿using Attache.Online.Interview.DomainEntities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace Attache.Online.Interview
{
    public class ItemRepository
    {
        public DbConnection dbConnection { get; set; }
        public ItemRepository()
        {            
        }

        public async Task SaveItem(Item item)
        {
            throw new NotImplementedException();
        }

        public async Task<Item[]> LoadItems(Func<Item, bool> filter)
        {
            throw new NotImplementedException();
        }

        public async Task<Item> GetItemById(Guid id)
        {
            throw new NotImplementedException();
        }

    }
}
