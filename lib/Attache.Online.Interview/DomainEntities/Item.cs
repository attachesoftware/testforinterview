﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Attache.Online.Interview.DomainEntities
{
    public class Item
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string OtherProperty { get; set; }
        public ItemStatus Status { get; set; } = ItemStatus.New;
    }

    public enum ItemStatus
    {
        New,                
        Updated,
        Old
    }
}
