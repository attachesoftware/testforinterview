﻿using AutoMapper;

namespace Attache.Online.Interview.Infrastructure
{
    /// <summary>
    /// Registers auto mapper mappings for organisation domain
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class AutoMappings : Profile
    {
    }
}