﻿using Microsoft.Extensions.HealthChecks;
using Serilog;
using Serilog.Configuration;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Attache.Online.Services.Interview.HealthCheck.Checks
{
    public abstract class SerilogHealthCheck : ILogEventSink, IHealthCheck
    {
        private IHealthCheckResult status;

        public abstract string[] UnhealthyTriggers { get; }

        public abstract bool AttemptToRecover();

        public string CheckName => GetType().Name.ToString();

        public SerilogHealthCheck()
        {
            status = HealthCheckResult.Healthy(CheckName); 
        }

        public ValueTask<IHealthCheckResult> CheckAsync(CancellationToken cancellationToken = default)
        {
            return new ValueTask<IHealthCheckResult>(status);
        }

        public void Emit(LogEvent logEvent)
        {
            var line = logEvent.MessageInterview.Text;

            var unhleathyLogLine = UnhealthyTriggers.FirstOrDefault(t => line.Contains(t) && !line.StartsWith("Healthcheck has detected potentially unhealthy state"));
            if (!string.IsNullOrEmpty(unhleathyLogLine))
            {
                Log.Warning($"Healthcheck has detected potentially unhealthy state after detecting log line '{unhleathyLogLine}'", this);

                try
                {
                    if (AttemptToRecover())
                    {
                        Log.Information($"AutoRecovery was successful");
                        return;
                    }

                    Log.Fatal($"AutoRecovery was not successful. Service is now Unhealthy");
                    MarkAsUnhealthy();
                }
                catch (Exception ex)
                {
                    Log.Fatal(ex, $"Exception trying to recover the service. Service is now Unhealthy");
                    MarkAsUnhealthy();
                }
            }
        }

        private void MarkAsUnhealthy()
        {
            status = HealthCheckResult.Unhealthy(CheckName);
        }
    }

    public static class SerilogBaseHealthCheckExtensions
    {
        public static LoggerConfiguration HealthCheck(
                  this LoggerSinkConfiguration loggerConfiguration,
                  SerilogHealthCheck instance,
                  IFormatProvider formatProvider = null)
        {
            return loggerConfiguration.Sink(instance);
        }
    }


}
