﻿using Serilog;
using Serilog.Configuration;
using Serilog.Core;
using System;
using System.Threading;

namespace Attache.Online.Services.Interview.HealthCheck.Checks
{
    public class RabbitMqHealthCheck : SerilogHealthCheck
    {
        private CancellationTokenSource abortControl;

        public RabbitMqHealthCheck(CancellationTokenSource abortControl) : base()
        {
            this.abortControl = abortControl;
        }

        public override string[] UnhealthyTriggers => new[] {"Operation interrupted",
                                                             "RabbitMQ Connect Failed: Operation interrupted", 
                                                             "RabbitMQ Connect Failed: Broker unreachable"};
        public override bool AttemptToRecover()
        {
            abortControl.Cancel();
            return false;
        }
    }
}
