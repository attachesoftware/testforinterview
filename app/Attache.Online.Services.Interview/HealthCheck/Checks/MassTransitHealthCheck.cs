﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Attache.Messaging;
using Attache.Online.Interview.ApplicationServices.Contracts.Queries;
using Microsoft.Extensions.HealthChecks;
using Polly;
using Polly.Caching;

namespace Attache.Online.Services.Interview.HealthCheck.Checks
{
    public class MassTransitHealthCheck : IHealthCheck
    {
        private readonly ICommandQueryEventBus bus;
        private readonly AsyncCachePolicy cachePolicy;

        public MassTransitHealthCheck(ICommandQueryEventBus bus)
        {
            this.bus = bus;

            Microsoft.Extensions.Caching.Memory.IMemoryCache memoryCache
                = new Microsoft.Extensions.Caching.Memory.MemoryCache(new Microsoft.Extensions.Caching.Memory.MemoryCacheOptions());
            Polly.Caching.Memory.MemoryCacheProvider memoryCacheProvider
                = new Polly.Caching.Memory.MemoryCacheProvider(memoryCache);

            cachePolicy = Policy.CacheAsync(memoryCacheProvider, TimeSpan.FromMinutes(3));
        }

        public async ValueTask<IHealthCheckResult> CheckAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            return await cachePolicy.ExecuteAsync(context => MassTransitHealthCheckResult(), new Context("HealthCheck"));
        }

        private async Task<IHealthCheckResult> MassTransitHealthCheckResult()
        {
            try
            {
                var response = await bus.ExecuteQuery<HealthCheckQuery, HealthCheckQueryRepsonse>(new HealthCheckQuery());
                return response.Message == HealthCheckQueryRepsonse.Success ? HealthCheckResult.Healthy(CheckName) : HealthCheckResult.Unhealthy(CheckName);
            }
            catch (Exception)
            {
                return HealthCheckResult.Unhealthy(CheckName);
            }
        }

        public string CheckName { get; } = nameof(MassTransitHealthCheckResult);
    }
}