﻿using System.Net;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Autofac.Extensions.DependencyInjection;
using System.Configuration;
using MassTransit;
using Attache.Online.Gateway.Filter;
using MySql.Data.MySqlClient;
using System;
using Serilog;
using Attache.Online.Services.Interview.HealthCheck.Checks;
using System.Threading;
using System.Threading.Tasks;
using Autofac;

namespace Attache.Online.Services.Interview
{
    public class Program
    {
        private static IBusControl inProcessBus;
        private static IBusControl reliableBus;

        public static void Main(string[] args)
        {
            var iShouldRun = ConfigurationManager.AppSettings["RunInterviewServicesInDocker"] == "true";
            try
            {
                var webHost = BuildWebHost(args);

                Log.Logger = new LoggerConfiguration()
                                .MinimumLevel.Debug()
                                .WriteTo.Console()
                                .WriteTo.HealthCheck((RabbitMqHealthCheck)webHost.Services.GetService(typeof(RabbitMqHealthCheck)))
                                .CreateLogger();

                var scope = (ILifetimeScope)webHost.Services.GetService(typeof(ILifetimeScope));
                inProcessBus = scope.ResolveNamed<IBusControl>("InProcessBusControl");
                reliableBus = scope.ResolveNamed<IBusControl>("ReliableBusControl");

                if (iShouldRun)
                {
                    RunStartupTests();

                    StartServices(webHost);
                }
                else
                {
                    Log.Information("Interview Services is not configured to start.");
                }

                var cancellationToken = (CancellationTokenSource)webHost.Services.GetService(typeof(CancellationTokenSource));
                Task.Run(async () => await webHost.RunAsync(cancellationToken.Token));
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Interview services failed to start");
                throw;
            }
        }

        private static void RunStartupTests()
        {
            // TODO: Add some interesting tests to check if safe to start.
            Log.Information("Running Startup Tests");
        }

        private static void StartServices(IWebHost webHost)
        {
            Log.Information("Starting Interview Services");

            PreloadDynamicTypes();

            reliableBus.ConnectPublishObserver((IPublishObserver)webHost.Services.GetService(typeof(SignalRResponsePublishObserver)));
            reliableBus.Start();

            inProcessBus.Start();

        }

        /// <summary>
        /// dotnetcore does not pre-load assemblies, so they are not available for dynamic loading
        /// by referencing the classes, the assemblies are available for dynamic loading.
        /// </summary>
        private static void PreloadDynamicTypes()
        {
            var x = new MySqlClientFactory();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseHealthChecks("/api/HealthCheck")
                .ConfigureServices(services => services.AddAutofac())
                .UseKestrel(options =>
                {
                    options.Listen(IPAddress.Any, 1000); //TODO: Change this to a unique port in the 5000-6000 range. Make sure it doesn't overlap with any existing containers.
                })
                .UseSerilog()
                .ConfigureLogging(logging => logging.SetMinimumLevel(LogLevel.Debug))
                .Build();
    }
}
