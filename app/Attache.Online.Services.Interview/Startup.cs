﻿using System;
using System.Configuration;
using Attache.Online.Authentication;
using Attache.Online.Authentication.IOC;
using Attache.Online.Authentication.Policy;
using Attache.Online.Gateway.IoC;
using Attache.Online.Services.Interview.HealthCheck.Checks;
using Attache.Online.Services.Interview.IoC;
using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;

namespace Attache.Online.Services.Interview
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var domain = ConfigurationManager.AppSettings["domain_services"];

            services.AddHttpClient();

            services.AddHealthChecks(checks =>
            {
                checks.AddCheck<RabbitMqHealthCheck>("RabbitMQ", TimeSpan.Zero);
                checks.AddCheck<MassTransitHealthCheck>("MassTransit", TimeSpan.Zero);
            });

            services.AddCors(options =>
            {
                options.AddPolicy("CORS",
                    builder => builder
                    .SetIsOriginAllowed(s => s.EndsWith($"{domain}"))
                    .AllowAnyMethod()
                    .AllowCredentials()
                    .AllowAnyHeader()
                    .Build());
            });

            var identityServer = ConfigurationManager.AppSettings["IdentityServer.Endpoint"];
            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = identityServer;
                    options.RequireHttpsMetadata = false;
                    options.Audience = "Interview";
                });

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddMvc();

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("CORS"));
            });

            services.AddLogging(options =>
            {
                options
                    .AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning);
            });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new InterviewServicesModule());
            builder.RegisterModule(new GatewayModule());
            builder.RegisterModule(new AuthenticationModule());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseAuthentication();
            app.UseAttacheOnlineAuthorisationMiddleware();

            /* Uncomment me if you're serving an angular app
            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404)
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            app.UseStaticFiles();
            */

            app.UseMvc();
            app.UseCors("CORS");
        }
    }
}