﻿using Attache.Messaging.Infrastructure;
using Attache.Online.Authentication.Middleware;
using Attache.Online.Framework.Configuration;
using Attache.Online.Framework.Cryptography;
using Attache.Online.Framework.Infrastructure;
using Attache.Online.Framework.Infrastructure.Services;
using Attache.Messaging.RabbitMq;
using Attache.Messaging.RabbitMq.Configuration;
using Attache.Online.Interview.ApplicationServices.CommandHandlers.Sample;
using Attache.Online.Interview.ApplicationServices.QueryHandlers.Sample;
using Autofac;
using Microsoft.Extensions.Logging;
using Attache.Online.Services.Interview.HealthCheck;
using Attache.Online.Services.Interview.HealthCheck.Checks;
using Attache.Online.Interview.ApplicationServices.Contracts.Queries;
using MassTransit;
using Attache.Online.Framework.Infrastructure.MessageDirectory;
using System;
using System.Collections.Generic;
using Attache.Online.Authentication.Policy;
using System.Threading;

namespace Attache.Online.Services.Interview.IoC
{
    public class InterviewServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Logging
            builder.Register(c =>
            {
                var loggerFactory = c.Resolve<ILoggerFactory>();
                return loggerFactory.CreateLogger("InterviewServices");
            }).AsImplementedInterfaces();

            // General Dependencies
            builder.RegisterType<WrappedConfigurationManager>().AsImplementedInterfaces();
            builder.RegisterType<DefaultBusSettings>().AsImplementedInterfaces();
            builder.RegisterType<SqlConnectionFactory>().AsImplementedInterfaces();
            builder.RegisterType<CacheValueServices>().AsImplementedInterfaces();
            builder.RegisterType<CryptographyServices>().AsImplementedInterfaces();

            // Messaging
            builder.RegisterType<CommandQueryEventBus>().AsImplementedInterfaces();
            builder.RegisterType<RabbitMqBus>().AsImplementedInterfaces();
            builder.RegisterType<ByConventionEndpointResolver>().AsImplementedInterfaces();
            builder.RegisterModule(new RabbitMqModule()
            {
                RegisterInProcessBus = true,
                CustomConfigurator = (c, cc) =>
                {
                    cc.UseAttacheAuthentication(c);
                    cc.UseExtensionsLogging(c.Resolve<ILoggerFactory>());
                }
            });

            builder.RegisterType<MessageDirectoryService>().AsImplementedInterfaces();

            // Register Handlers
            builder.RegisterType<SampleQueryHandler>().AsImplementedInterfaces();
            builder.RegisterType<SampleCommandHandler>().AsImplementedInterfaces();

            // Health Checks 
            builder.RegisterType<RabbitMqHealthCheck>().As<SerilogHealthCheck>().AsSelf().SingleInstance(); 
			builder.RegisterType<MassTransitHealthCheck>().AsImplementedInterfaces().AsSelf().SingleInstance();

            // Global stop button
            builder.Register((c) => new CancellationTokenSource()).AsSelf().SingleInstance();
        }
    }
}