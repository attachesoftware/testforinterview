﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Attache.Online.Services.Interview.Model
{
    public class QueryPayload
    {
        public string QueryType { get; set; }
        public string ResponseType { get; set; }
        public string Data { get; set; }
    }
}