﻿using Newtonsoft.Json;

namespace Attache.Online.Services.Interview.Model
{
    public class CommandPayload
    {
        [JsonProperty("Type")]
        public string CommandType { get; set; }

        public string Data { get; set; }
    }
}