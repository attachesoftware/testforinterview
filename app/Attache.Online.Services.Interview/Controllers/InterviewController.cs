﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Attache.Messaging;
using Attache.Messaging.RabbitMq;
using Attache.Online.Authentication;
using Attache.Online.Framework.Configuration;
using Attache.Online.Framework.Infrastructure.MessageDirectory;
using Attache.Online.Services.Interview.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static Attache.Messaging.RabbitMq.RabbitMqCommandQueryEventBusFactory;

namespace Attache.Online.Services.Interview.Controllers
{
    public class InterviewController : Controller
    {
        private readonly RabbitMqCommandQueryEventBusFactory busFactory;
        private readonly IConfigurationManager configurationManager;
        private readonly IMessageDirectoryService messageDirectoryService;

        public InterviewController(RabbitMqCommandQueryEventBusFactory busFactory,
            IConfigurationManager configurationManager,
            IMessageDirectoryService messageDirectoryService)
        {
            this.busFactory = busFactory;
            this.configurationManager = configurationManager;
            this.messageDirectoryService = messageDirectoryService;
        }

        [HttpPost]
        [Route("api/Interview/Command")]
        public async Task Command([FromBody]CommandPayload payload)
        {
            var convertToType = this.messageDirectoryService.GetMessageDetailsByName(payload.CommandType).Type;
            var command = JsonConvert.DeserializeObject(payload.Data, convertToType);

            await this.busFactory.Bus(Mode.Reliable).ExecuteCommand(command);
        }

        [HttpPost]
        [Route("api/Interview/Query")]
        public async Task<JObject> Query([FromBody]QueryPayload payload)
        {
            var messageDetails = this.messageDirectoryService.GetMessageDetailsByName(payload.QueryType);
            var queryType = messageDetails.Type;
            var responseType = messageDetails.ResponseType ?? this.messageDirectoryService.GetMessageDetailsByName(payload.ResponseType).Type;

            var query = JsonConvert.DeserializeObject(payload.Data, queryType);

            var result = await this.busFactory.Bus(Mode.InProcess).ExecuteQuery(responseType, query);

            if (result == null)
                return null;

            return JObject.FromObject(result);
        }
    }
}