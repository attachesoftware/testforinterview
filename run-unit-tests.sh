#!/bin/bash

mkdir -p /output/testResults

dotnet vstest /test/Interview/Attache.Online.Interview.Tests.dll /test/Interview/Attache.Online.Interview.ApplicationServices.Tests.dll /test/Interview/Attache.Online.Interview.Infrastructure.Tests.dll --logger:"trx;LogFileName=domain_tests.xml" --ResultsDirectory:/output/testResults 

#TODO: How to work out if tests have failed?

chmod -R 0777 /output/testResults

echo "All tests completed successfully"

cat /output/testResults/domain_tests.xml

