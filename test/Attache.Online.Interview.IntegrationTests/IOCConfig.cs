﻿using System;
using System.Configuration;
using System.Reflection;
using Attache.Online.Framework.Infrastructure;
using Attache.Online.Framework.Interfaces;
using Attache.Online.Interview.Infrastructure.Repository;
using Autofac;
using MySql.Data.MySqlClient;

namespace Attache.Online.Interview.IntegrationTests
{
    internal class IOCConfig
    {
        internal static IContainer CreateContainer()
        {
            PreloadDynamicTypes();

            var builder = new ContainerBuilder();

            // This is required because apparently dotnet test can't find the app config of assemblies.
            builder.Register<IDBConnectionFactory>(c => {
                var sqlFactory = new SqlConnectionFactory();
                sqlFactory.ConnectionString = (key) =>
                {
                    var debugPath = new Uri(Assembly.GetAssembly(typeof(IOCConfig)).CodeBase).AbsolutePath;
                    var config = ConfigurationManager.OpenMappedExeConfiguration(new ExeConfigurationFileMap()
                    {
                        ExeConfigFilename = debugPath + ".config"
                    }, ConfigurationUserLevel.None);

                    return config.ConnectionStrings.ConnectionStrings[key];
                };
                return sqlFactory;
            })
            .AsImplementedInterfaces();

            builder.RegisterType<EmployeeDomainRepository>().AsSelf();

            return builder.Build();
        }

        private static void PreloadDynamicTypes()
        {
            var x = new MySqlClientFactory();
        }
    }
}