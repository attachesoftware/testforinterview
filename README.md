attache-service-interview

To install this interview, run a new powershell script in root of repo:

dotnet new -i {path-to-repository}

eg:
dotnet new -i C:\Dev\attache-service-interview

You should see a new interview:

Attache Online Microservice                       attache-service      [C#]              Web/API/Docker

To uninstall:

dotnet new -u {path-to-repository}

eg:
dotnet new -u C:\Dev\attache-service-interview

To Create a new Service based on the interview:

1. for some stupid reason, close all Visual Studio instances.
1. cd to the parent folder of where the repository will exist
2. dotnet new attache-service --name MyNewService --port {port} --dbpassword {password}

where
port is a port not in use by any other services.
password is a strong password to be used for the appUser login