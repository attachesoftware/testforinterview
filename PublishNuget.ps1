﻿Param(
	[string]$target
)

$attacheNuGetServer = "https://nuget.tools.aws.attachecloud.com/"
$apiKey = "Attache"

Function dotnet-nuget-push([string] $path) {
	del $path/bin/Release/*.nupkg
	dotnet build -c Release $path
	Push-Location -Path $path/bin/Release
	dotnet nuget push *.nupkg -s $attacheNuGetServer -k $apiKey
	Pop-Location
}

Write-Host "1. Attache.Online.Interview.ApplicationServices.Contracts"

$selection = Read-Host -Prompt "Choose a Package to publish"

switch($selection)
{
    1 {dotnet-nuget-push("lib/Attache.Online.Interview.ApplicationServices.Contracts")}
}