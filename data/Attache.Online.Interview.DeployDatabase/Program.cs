﻿using Dapper;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Attache.Online.Interview.DeployDatabase
{
    class Program
    {
        private static string rhPathConfig;
        private static string workingPath;
        private static string rhPath;
        private static ConnectionStringSettings connectionString;

        static void Main(string[] args)
        {
            try
            {
                var iShouldRun = ConfigurationManager.AppSettings["DeployDatabase"] == "true";

                if (!iShouldRun)
                {
                    Console.WriteLine("DeployDatabase is configured to false. Nothing to do... Exiting.");
                    Environment.Exit(0);
                }

                rhPathConfig = args.Length > 0 ? args[0] : "/rh";

                workingPath = Path.GetFullPath(Directory.GetCurrentDirectory());
                rhPath = Path.GetFullPath(workingPath + rhPathConfig);

                Console.WriteLine($"Working Path: {workingPath}");
                Console.WriteLine($"Roundhouse Path: {rhPath}");

                var dropCreateParam = ConfigurationManager.AppSettings["DB-DropCreateFor"];
                var outputFolderParam = ConfigurationManager.AppSettings["OutputPath"];
                var dropCreateTargets = dropCreateParam.Split(',');

                var target = "Interview";

                if (!File.Exists($"{rhPath}/rh.dll"))
                {
                    Console.Write($"Roundhouse not found at {rhPath}");
                    Environment.Exit(1);
                }

                try
                {
                    connectionString = ConfigurationManager.ConnectionStrings[$"Deploy.{target}"];
                }
                catch
                {
                    Console.Write(@"No Configuration found for target {0}", target);
                    Environment.Exit(1);
                }

                var db = GetDatabaseFromConnectionString(connectionString);

                var kickedVersion = Assembly.GetExecutingAssembly().GetName().Version;
                if (WeAreAlreadyAtVersion(kickedVersion))
                {
                    Console.Write($"We are already up to date. Nothing to do.");
                    Environment.Exit(0);
                }

                var databaseType = "MySQL";

                var rhArgs = @"--noninteractive " +
                            $@"--databasename={db} " +
                            $@"--connectionstring=""{connectionString}"" " +
                            $@"--databasetype={databaseType} " +
                            $@"--version=""{kickedVersion}"" " +
                            $@"--sqlfilesdirectory={target} " +
                            $@"--outputpath={outputFolderParam} " +
                            $@"--environmentname=""{ConfigurationManager.AppSettings["environment_name"]}"" ";

                if (!DropDatabase(target, rhArgs, dropCreateTargets) || RoundhouseKick(rhArgs) != 0)
                {
                    if (Debugger.IsAttached)
                        Debugger.Break();
                    Environment.Exit(1);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0}", ex.Message);
                Console.WriteLine("{0}", ex.StackTrace);

                if (Debugger.IsAttached)
                    Debugger.Break();

                Environment.Exit(1);
            }
        }

        private static bool WeAreAlreadyAtVersion(Version kickedVersion)
        {
            try
            {
                using (var connection = new MySqlClientFactory().CreateConnection())
                {
                    connection.ConnectionString = connectionString.ConnectionString;
                    connection.Open();

                    var currentVersionString = connection.Query<string>("select version from roundhouse_version order by id desc limit 1").FirstOrDefault();
                    var currentVersion = new Version(currentVersionString);

                    Console.WriteLine($"Current database version is {currentVersion}");
                    Console.WriteLine($"New database version is {kickedVersion}");

                    //Console.WriteLine($"Major: {currentVersion.Major} >= {kickedVersion.Major} == {currentVersion.Major >= kickedVersion.Major}");
                    //Console.WriteLine($"MajorRevision: {currentVersion.MajorRevision} >= {kickedVersion.MajorRevision} == {currentVersion.MajorRevision >= kickedVersion.MajorRevision}");
                    //Console.WriteLine($"Minor: {currentVersion.Minor} >= {kickedVersion.Minor} == {currentVersion.Minor >= kickedVersion.Minor}");
                    //Console.WriteLine($"MinorRevision: {currentVersion.MinorRevision} >= {kickedVersion.MinorRevision} == {currentVersion.MinorRevision >= kickedVersion.MinorRevision}");

                    return currentVersion == kickedVersion;
                }
            }
            catch(Exception ex)
            {
                if (ex.Message.Contains("Unknown database"))
                {
                    return false; // database does not exist, so clearly we're not at that version 
                }

                throw;
            }
        }

        private static string GetDatabaseFromConnectionString(ConnectionStringSettings connectionString)
        {
            var sections = connectionString.ConnectionString.Split(';');
            var dbSection = sections.FirstOrDefault(s => s.StartsWith("database"));
            return dbSection.Split('=')[1];
        }

        private static bool DropDatabase(string target, string args, IEnumerable<string> dropCreateTargets)
        {
            if (dropCreateTargets.Contains(target, StringComparer.InvariantCultureIgnoreCase))
            {
                return RoundhouseKick($"{args} --drop") == 0;
            }

            return true;
        }

        private static int RoundhouseKick(string rhArgs)
        {
            var p = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    UseShellExecute = false,
                    WorkingDirectory = $"{workingPath}",
                    FileName = @"dotnet",
                    Arguments = $"{rhPath}/rh.dll " + rhArgs
                }
            };

            p.Start();

            p.StartInfo.RedirectStandardOutput = true;
            p.OutputDataReceived += (sender, data) => {
                Console.WriteLine(data.Data);
            };

            p.StartInfo.RedirectStandardError = true;
            p.ErrorDataReceived += (sender, data) => {
                Console.WriteLine(data.Data);
            };
            
            p.WaitForExit();

            return p.ExitCode;
        }
    }
}
