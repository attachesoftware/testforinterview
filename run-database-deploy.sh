#!/bin/bash

echo "> run-database-deploy.sh"

echo "*******************************************************************"
echo "*                                                                 *"
echo "*                  Running Database Deploy...                     *"
echo "*                                                                 *"
echo "*******************************************************************"

configtool="/ext/configtool/Attache.Online.ConfigTool.DotNetCore.dll"

command -v dotnet >/dev/null 2>&1 || { echo >&2 ".NET Core is not installed! Exiting..."; exit 1; }

if [ ! -f $configtool ]; then
    echo "ConfigTool executable was not found! Exiting..."
	exit 1
fi

if [ -z $ENVIRONMENT ]; then
	echo "ENVIRONMENT variable is not set! Exiting..."
	exit 1
fi


echo "Running config tool on DeployDatabase..."
dotnet $configtool $ENVIRONMENT /app/data Attache.Online.Interview.DeployDatabase.dll.config
if [[ $? -ne 0 ]]; then
	echo "Failed to run config tool! Exiting..."
	exit 1
fi

echo "Starting DeployDatabase..."
cd /app/data
mkdir Output
dotnet Attache.Online.Interview.DeployDatabase.dll
if [[ $? -ne 0 ]]; then
	dberror = "1"
fi

echo ""
echo "*******************************************************************"
echo "*                    Database Migration Logs                       *"
echo "*******************************************************************"
cd Output
find . -type f -name *.log -exec cat {} \;
echo "*******************************************************************"

if [[ -z $dberror ]]; then
	echo "DeployDatabase completed successfully."
else
	echo "DeployDatabase Failed!"
	exit 1
fi
