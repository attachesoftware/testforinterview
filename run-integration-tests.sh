#!/bin/bash

mkdir -p /output/testResults

echo "*******************************************************************"
echo "*                                                                 *"
echo "*                       INTEGRATION TESTS                         *"
echo "*                                                                 *"
echo "*******************************************************************"

configtool="/ext/configtool/Attache.Online.ConfigTool.DotNetCore.dll"

command -v dotnet >/dev/null 2>&1 || { echo >&2 ".NET Core is not installed! Exiting..."; exit 1; }

if [ ! -f $configtool ]; then
    echo "ConfigTool executable was not found! Exiting..."
	exit 1
fi

if [ -z $ENVIRONMENT ]; then
	echo "ENVIRONMENT variable is not set! Exiting..."
	exit 1
fi

echo "Running config tool on Integration Tests..."
dotnet $configtool $ENVIRONMENT /test/integration Attache.Online.Interview.IntegrationTests.dll.config
if [[ $? -ne 0 ]]; then
	echo "Failed to run config tool! Exiting..."
	exit 1
fi

dotnet vstest /test/integration/Attache.Online.Interview.IntegrationTests.dll dotnet vstest /test/integration/Attache.Online.Interview.AcceptanceTests.dll --logger:"trx;LogFileName=integration_tests.xml" --ResultsDirectory:/output/testResults

chmod -R 0777 /output/testResults

echo "All tests completed successfully"
